package com.li.module.mapper;

import com.li.module.vo.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommentMapper extends JpaRepository<Comment, Integer> {
}
