package com.li.module.mapper;

import com.li.module.vo.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionMapper extends JpaRepository<Permission,Integer> {
}
