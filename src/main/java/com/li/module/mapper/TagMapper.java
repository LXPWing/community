package com.li.module.mapper;

import com.li.module.vo.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-25 16:33
 **/
@Repository
public interface TagMapper extends JpaRepository<Tag, Integer> {
}
