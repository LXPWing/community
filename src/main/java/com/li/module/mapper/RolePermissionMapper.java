package com.li.module.mapper;

import com.li.module.vo.RolePermission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePermissionMapper extends JpaRepository<RolePermission,Integer> {
}
