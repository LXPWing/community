package com.li.module.mapper;

import com.li.module.vo.Topic;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicMapper extends JpaRepository<Topic,Integer> {
}
