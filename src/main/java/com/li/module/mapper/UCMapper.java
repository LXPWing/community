package com.li.module.mapper;

import com.li.module.vo.User;
import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UCMapper extends JpaRepository<User,Integer> {
    User findById(int id);

    User findByToken(String token);

    User findByUsername(String username);

    User findByEmail(String email);
}
