package com.li.module.mapper;

import com.li.module.vo.TopicTag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TopicTagMapper extends JpaRepository<TopicTag,Integer> {
}
