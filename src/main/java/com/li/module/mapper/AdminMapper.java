package com.li.module.mapper;

import com.li.module.vo.AdminUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdminMapper extends JpaRepository<AdminUser,Integer> {

    AdminUser findByUsername(String usernmae);

    //AdminUser findById(Integer id);

    List<AdminUser> findByRoleId(Integer id);
}
