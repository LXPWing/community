package com.li.module.service;

import com.li.module.vo.Role;

import java.util.List;

public interface RoleService {

    Role selectById(Integer roleId);

    List<Role> selectAll();

    void insert(String name, Integer[] permissionIds);

    void update(Integer id, String name, Integer[] permissionIds);

    void delete(Integer id);
}
