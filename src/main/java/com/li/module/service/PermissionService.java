package com.li.module.service;

import com.li.module.vo.Permission;

import java.util.List;
import java.util.Map;

public interface PermissionService {
    /**
     * 根据RoleId查询权限集合
     * @param id
     * @return
     */
    List<Permission> selectByRoleId(Integer id);

    /**
     * 查询Role的全部权限
     * @return
     */
    Map<String,List<Permission>> selectAll();

    Permission update(Permission permission);

    Permission insert(Permission permission);

    void delete(Integer id);

}
