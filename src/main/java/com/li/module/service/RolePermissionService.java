package com.li.module.service;

import com.li.module.vo.RolePermission;

import java.util.List;

public interface RolePermissionService {
    List<RolePermission> selectById(Integer id);

    void deleteByRoleId(Integer id);

    void deleteByPermissionId(Integer id);

    void insert(RolePermission rolePermission);
}
