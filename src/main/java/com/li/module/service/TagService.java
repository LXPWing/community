package com.li.module.service;

import com.li.module.vo.Tag;

import java.util.List;

public interface TagService {
    Tag selectById(Integer id);

    Tag selectByName(String name);

    List<Tag> selectByIds(List<Integer> id);

    List<Tag> selectByTopicId(Integer id);
}
