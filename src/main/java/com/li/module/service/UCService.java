package com.li.module.service;

import com.li.module.vo.User;

import java.util.List;

public interface UCService {
    /**根据id查询用户*/
    User selectById(int id);

    /**根据token查询用户*/
    User selectByToken(String token);

    User selectByUsername(String username);

    User selectByEmail(String email);

    User addUser(String username,String password,String email);

    List<User> selectTop(Integer limit);

    void updateUser(User user);

    void deleteUser(Integer id);

    void deleteRedisUser(User user);
}
