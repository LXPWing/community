package com.li.module.service;

import com.li.module.vo.AdminUser;

import java.util.List;
import java.util.Map;

public interface AdminUserService {
    /**
     * 根据用户名查询
     * @param usernmae
     * @return
     */
    AdminUser selectByUsername(String usernmae);

    /**
     * 查询所有的后台用户
     * @return
     */
    List<AdminUser> selectAll();

    /**
     * 更新后台用户
     * @param adminUser
     */
    void update(AdminUser adminUser);

    /**
     * 删除后台用户
     * @param id
     */
    void delete(Integer id);

    /**
     * 插入后台管理员
     * @param adminUser
     */
    void insert(AdminUser adminUser);

    /**
     * 根据id查询后台管理员
     * @param id
     * @return
     */
    AdminUser selectById(Integer id);

    /**
     * 根据角色id查询后台关联的用户
     * @param id
     * @return
     */
    List<AdminUser> selectByRoleId(Integer id);
}
