package com.li.module.service;

import com.li.module.vo.Tag;
import com.li.module.vo.TopicTag;

import java.util.List;

public interface TopicTagService {

    List<TopicTag> selectByTopicId(Integer topicId);

    List<TopicTag> selectByTagId(Integer tagId);

    void insertTopicTag(Integer topicId, List<Tag> tagList);

    // 删除话题所有关联的标签记录
    void deleteByTopicId(Integer id);

}
