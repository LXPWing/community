package com.li.module.service.Impl;

import com.li.module.service.TopicService;
import com.li.module.vo.Topic;
import com.li.module.vo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:21
 **/
@Service
public class TopicTagServiceImpl implements TopicService {
    private Logger logger= LoggerFactory.getLogger(TopicTagServiceImpl.class);

    @Override
    public List<Topic> selectAuthorOtherTopic(Integer userId, Integer topicId, Integer limit) {
        return null;
    }

    @Override
    public Topic insert(String title, String content, String tags, User user) {
        return null;
    }

    @Override
    public Topic selectById(Integer id) {
        return null;
    }

    @Override
    public Topic selectByTitle(String title) {
        return null;
    }

    @Override
    public Topic updateViewCount(Topic topic, String ip) {
        return null;
    }

    @Override
    public void update(Topic topic, String tags) {

    }

    @Override
    public void delete(Topic topic) {

    }

    @Override
    public void deleteByUserId(Integer userId) {

    }

    @Override
    public int countToday() {
        return 0;
    }
}
