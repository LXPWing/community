package com.li.module.service.Impl;

import com.li.module.mapper.AdminMapper;
import com.li.module.service.AdminUserService;
import com.li.module.service.UCService;
import com.li.module.vo.AdminUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:18
 **/
@Service
public class AdminUserServiceImpl implements AdminUserService {
    private Logger logger= LoggerFactory.getLogger(AdminUserService.class);

    @Autowired
    private AdminMapper adminMapper;

    @Override
    public AdminUser selectByUsername(String usernmae) {
        return adminMapper.findByUsername(usernmae);
    }

    @Override
    public List<AdminUser> selectAll() {
       return adminMapper.findAll();
    }

    @Override
    public void update(AdminUser adminUser) {
        adminMapper.save(adminUser);
    }

    @Override
    public void delete(Integer id) {
        adminMapper.deleteById(id);
    }

    @Override
    public void insert(AdminUser adminUser) {
        adminMapper.save(adminUser);
    }

    @Override
    public AdminUser selectById(Integer id) {
        Optional<AdminUser> byId = adminMapper.findById(id);
        return byId.get();
    }

    @Override
    public List<AdminUser> selectByRoleId(Integer id) {
        return adminMapper.findByRoleId(id);
    }
}
