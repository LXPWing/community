package com.li.module.service.Impl;

import com.li.module.service.TagService;
import com.li.module.vo.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:20
 **/
@Service
public class TagServiceImpl implements TagService {
    private Logger logger= LoggerFactory.getLogger(TagServiceImpl.class);

    @Override
    public Tag selectById(Integer id) {
        return null;
    }

    @Override
    public Tag selectByName(String name) {
        return null;
    }

    @Override
    public List<Tag> selectByIds(List<Integer> id) {
        return null;
    }

    @Override
    public List<Tag> selectByTopicId(Integer id) {
        return null;
    }
}
