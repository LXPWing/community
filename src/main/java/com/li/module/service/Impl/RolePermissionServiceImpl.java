package com.li.module.service.Impl;

import com.li.module.service.RolePermissionService;
import com.li.module.vo.RolePermission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:23
 **/
@Service
public class RolePermissionServiceImpl implements RolePermissionService {
    private Logger logger= LoggerFactory.getLogger(RolePermissionServiceImpl.class);

    @Override
    public List<RolePermission> selectById(Integer id) {
        return null;
    }

    @Override
    public void deleteByRoleId(Integer id) {

    }

    @Override
    public void deleteByPermissionId(Integer id) {

    }

    @Override
    public void insert(RolePermission rolePermission) {

    }
}
