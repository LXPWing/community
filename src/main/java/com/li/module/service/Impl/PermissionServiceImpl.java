package com.li.module.service.Impl;

import com.li.module.service.PermissionService;
import com.li.module.vo.Permission;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:19
 **/
@Service
public class PermissionServiceImpl implements PermissionService {
    private Logger logger= LoggerFactory.getLogger(PermissionServiceImpl.class);

    @Override
    public List<Permission> selectByRoleId(Integer id) {
        return null;
    }

    @Override
    public Map<String, List<Permission>> selectAll() {
        return null;
    }

    @Override
    public Permission update(Permission permission) {
        return null;
    }

    @Override
    public Permission insert(Permission permission) {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }
}
