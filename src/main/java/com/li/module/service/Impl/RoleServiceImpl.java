package com.li.module.service.Impl;

import com.li.module.service.RoleService;
import com.li.module.vo.Role;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:18
 **/
@Service
public class RoleServiceImpl implements RoleService {
    private Logger logger= LoggerFactory.getLogger(RoleServiceImpl.class);

    @Override
    public Role selectById(Integer roleId) {
        return null;
    }

    @Override
    public List<Role> selectAll() {
        return null;
    }

    @Override
    public void insert(String name, Integer[] permissionIds) {

    }

    @Override
    public void update(Integer id, String name, Integer[] permissionIds) {

    }

    @Override
    public void delete(Integer id) {

    }
}
