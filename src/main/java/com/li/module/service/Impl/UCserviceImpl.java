package com.li.module.service.Impl;

import com.li.common.exception.ApiAssert;
import com.li.module.mapper.UCMapper;
import com.li.module.service.UCService;
import com.li.module.vo.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-16 16:26
 **/
@Service
public class UCserviceImpl implements UCService {
    private Logger logger= LoggerFactory.getLogger(UCserviceImpl.class);

    @Autowired
    private UCMapper ucMapper;

    @Override
    public User selectById(int id) {
        User userById = ucMapper.findById(id);
        return userById;
    }

    @Override
    public User selectByToken(String token) {
         return ucMapper.findByToken(token);
    }

    @Override
    public User selectByUsername(String username) {
        return ucMapper.findByUsername(username);
    }

    @Override
    public User selectByEmail(String email) {
        return ucMapper.findByEmail(email);
    }

    @Override
    public User addUser(String username, String password, String email) {
//        User user = new User()
//                .setUsername(username)
//                .setPassword(password)
//                .setEmail(email);
//
//        ucMapper.save(user)
        return null;
    }

    @Override
    public List<User> selectTop(Integer limit) {
        return null;
    }

    @Override
    public void updateUser(User user) {
        ucMapper.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        User user=selectById(id);
        ApiAssert.notNull(user,"数据库中不存在此对象");
        user.setIsDelete(1);
        ucMapper.save(user);
    }

    @Override
    public void deleteRedisUser(User user) {

    }
}
