package com.li.module.service;

import com.li.module.vo.Tag;
import com.li.module.vo.Topic;
import com.li.module.vo.User;

import java.util.List;

public interface TopicService {

    List<Topic> selectAuthorOtherTopic(Integer userId, Integer topicId, Integer limit);

    Topic insert(String title, String content, String tags, User user);

    Topic selectById(Integer id);

    // 根据title查询话题，防止重复话题
    Topic selectByTitle(String title);

    // 处理话题的访问量
    Topic updateViewCount(Topic topic, String ip);

    // 更新话题
    void update(Topic topic, String tags);

    // 删除话题
    void delete(Topic topic);

    // 根据用户id删除帖子
    void deleteByUserId(Integer userId);

    // 查询今天新增的话题数
    int countToday();
}
