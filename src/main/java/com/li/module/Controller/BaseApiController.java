package com.li.module.Controller;

import com.li.common.domain.AjaxResult;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 梦哟i
 * @date : 2021-05-12 20:02
 **/
public class BaseApiController {

    protected AjaxResult success(){
        return success(null);
    }

    protected AjaxResult success(Object o){
        return AjaxResult.success(o);
    }

    protected AjaxResult error(String msg){
        return AjaxResult.error(msg);
    }

    
}
