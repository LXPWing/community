package com.li.module.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-20 22:47
 **/
@Data
@Entity
@Table(name = "topic")
@Accessors(chain = true)
public class Topic implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "context")
    private String context;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "user_id")
    private Integer userId;

}
