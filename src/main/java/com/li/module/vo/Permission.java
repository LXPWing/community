package com.li.module.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-20 22:07
 **/
@Data
@Entity
@Table(name = "permisson")
@Accessors(chain = true)
public class Permission implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "value")
    private String value;

    @Column(name = "permissionCode")
    private Integer permissionCode;
}
