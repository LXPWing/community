package com.li.module.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-16 15:10
 **/
@Data
@Entity
@Table(name = "user")
@Accessors(chain = true)
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "username")
    private String username;

    @Column(name="email")
    private String email;

    @Column(name = "avater")
    private String avatar;

    @Column(name = "bio")
    private String bio;

    @JsonIgnore
    @Column(name = "password")
    private String password;

    @JsonIgnore
    @Column(name = "token")
    private String token;

    @Column(name = "update_time")
    private Date updateTime;

    @Column(name = "active")
    private Boolean active;

    @Column(name="is_delete")
    private Integer isDelete;
}
