package com.li.module.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-18 21:24
 **/
@Data
@Entity
@Table(name = "admin")
@Accessors(chain = true)
public class AdminUser implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name="update_time")
    private String username;

    @Column(name = "password")
    private String password;

   @Column(name = "update_time",updatable = false,insertable = false)
    private Date updateTime;

    @Column(name="role_id")
    private Integer roleId;
}
