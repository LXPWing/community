package com.li.module.vo;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-20 22:34
 **/
@Data
@Entity
@Table(name = "comment")
@Accessors(chain = true)
public class Comment implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "content")
    private String content;

    @Column(name = "user_id")
    private Integer userId;

    @Column(name="update_time")
    private Date updateTime;

    @Column(name="topic_id")
    private Integer topicId;

    @Column(name="comment_id")
    private Integer commentId;

    @Column(name = "state")
    private Integer state;
}
