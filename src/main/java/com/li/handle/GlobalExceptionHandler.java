package com.li.handle;

import com.li.common.domain.AjaxResult;
import com.li.common.exception.BaseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-12 18:19
 **/
@RestControllerAdvice
public class GlobalExceptionHandler{
    private static final Logger log= LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 基础异常
     * @param e
     * @return
     */
    @ExceptionHandler(BaseException.class)
    public AjaxResult baseException(BaseException e){
        return AjaxResult.error(e.getMessage());
    }

    /**
     * 方法参数异常
     * @param e
     * @return
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public AjaxResult methodVailException(MethodArgumentNotValidException e){
        ArrayList<FieldError> list = new ArrayList<>();
        BindingResult bindingResult = e.getBindingResult();
        final List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        for(FieldError error: fieldErrors){
            list.add(error);
        }
        return AjaxResult.error("错误列表",list);
    }


}
