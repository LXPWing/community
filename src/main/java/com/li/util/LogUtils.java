package com.li.util;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-25 18:40
 **/
public class LogUtils {

    public static String getBlock(Object msg){
        if( msg == null){
            msg="";
        }
        return "[" + msg.toString() + "]";
    }

}
