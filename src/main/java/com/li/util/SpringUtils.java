package com.li.util;

import cn.hutool.core.util.StrUtil;
import org.springframework.aop.framework.AopContext;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-26 20:43
 **/
@Component
public class SpringUtils implements BeanFactoryPostProcessor , ApplicationContextAware {
    private static ConfigurableListableBeanFactory beanFactory;

    private static ApplicationContext applicationContext;

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
        SpringUtils.beanFactory= configurableListableBeanFactory;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        SpringUtils.applicationContext=applicationContext;
    }

    public static <T> T getBean(String name) throws BeansException{
        return (T)beanFactory.getBean(name);
    }

    public static <T> T getBean(Class<T> clz) throws BeansException{
        return (T) beanFactory.getBean(clz);
    }

    public static boolean containsBean(String name){
        return beanFactory.containsBean(name);
    }

    public static boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
        return beanFactory.isSingleton(name);
    }

    public static Class<?> getType(String name) throws NoSuchBeanDefinitionException{
        return beanFactory.getType(name);
    }

    public static String[] getAliases(String name) throws NoSuchBeanDefinitionException{
        return beanFactory.getAliases(name);
    }

    public static <T> T getAopProxy(T invoker){
        return (T)AopContext.currentProxy();
    }

    public static String[] getActiveProfiles(){
        return applicationContext.getEnvironment().getActiveProfiles();
    }

    public static String getActiveProfile(){
        final String[] activeProfile = getActiveProfiles();
        return StringUtils.isNotEmpty(activeProfile)?activeProfile[0]:null;
    }
}
