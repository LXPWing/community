package com.li.util;

import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-25 19:42
 **/
public class StringUtils extends cn.hutool.core.util.StrUtil{

    public static boolean isNotEmpty(String[] str){
        if(str == null){
            return false;
        }
        return true;
    }


    public static boolean inStringIgnoreCase(String str, String... strs){
        if(str != null && strs != null){
            for(String s : strs){
                if(str.equalsIgnoreCase(trim(s))){
                    return true;
                }
            }
        }
        return false;
    }

}
