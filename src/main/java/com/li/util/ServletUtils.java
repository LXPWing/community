package com.li.util;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-25 18:40
 **/
public class ServletUtils {

    /*获取Requst属性*/
    public static ServletRequestAttributes getRequestAttributes(){
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes)attributes;
    }

    /*获取Response*/
    public static HttpServletResponse getResponse(){
        return getRequestAttributes().getResponse();
    }

    /*获取Request*/
    public static HttpServletRequest getRequset(){
        return getRequestAttributes().getRequest();
    }

    /*获取session*/
    public static HttpSession getSession(){
        return getRequset().getSession();
    }

    /*获取String参数*/
    public static String getParameter(String name){
        return getRequset().getParameter(name);
    }

    /*判断是否为Ajax异步请求*/
    public static boolean isAjaxRequest(HttpServletRequest httpServletRequest){

        String accept = httpServletRequest.getHeader("accept");
        if(accept != null && accept.indexOf("application/json") != -1){
            return true;
        }

        String xRequestedWith = httpServletRequest.getHeader("X-Requested-With");
        if(xRequestedWith != null && xRequestedWith.indexOf("XMLHttpRequest") != -1){
            return true;
        }

        String uri=httpServletRequest.getRequestURI();
        if(StringUtils.inStringIgnoreCase(uri,".json",".xml")){
            return true;
        }

        String ajax = httpServletRequest.getParameter("__ajax");
        if (StringUtils.inStringIgnoreCase(ajax, "json", "xml"))
        {
            return true;
        }
        return false;

    }
}
