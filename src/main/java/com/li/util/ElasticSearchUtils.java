package com.li.util;

import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.io.IOException;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-15 15:48
 **/
public class ElasticSearchUtils {
    @Autowired
    private RestHighLevelClient restHighLevelClient;

    public void deleteMassage() throws IOException {
        DeleteRequest deleteRequest=new DeleteRequest();
        restHighLevelClient.delete(deleteRequest, RequestOptions.DEFAULT);
    }
}
