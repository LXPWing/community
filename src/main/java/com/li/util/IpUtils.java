package com.li.util;

import cn.hutool.core.util.StrUtil;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * <h3>community</h3>
 * <p>获取ip的方法</p>
 *
 * @author : 李星鹏
 * @date : 2021-04-25 16:44
 **/
public class IpUtils {

    public static String getIpAddr(HttpServletRequest httpServletRequst){
        if(httpServletRequst == null){
            return "unknown";
        }

        String ip = httpServletRequst.getHeader("X-Forwarded-For");
        String sourceIp = null;

        if(ip == null || ip.length()==0 || "unknown".equalsIgnoreCase(ip)){
            ip=httpServletRequst.getHeader("Proxy-Client-IP");
        }

        if(ip == null || ip.length()==0 || "unknown".equalsIgnoreCase(ip)){
            ip=httpServletRequst.getHeader("WL- Proxy-Client-IP");
        }

        if(ip == null || ip.length()==0 || "unknown".equalsIgnoreCase(ip)){
            ip=httpServletRequst.getHeader("HTTP_CLIENT_IP");
        }

        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip=httpServletRequst.getHeader("X-Real-IP");
        }

        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = httpServletRequst.getHeader("X-Forwarded-For");
        }

        if(ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip=httpServletRequst.getRemoteAddr();
        }

        if(!StrUtil.isEmpty(ip)){
            sourceIp = ip.split(",")[0];
        }

        return sourceIp;
    }


}
