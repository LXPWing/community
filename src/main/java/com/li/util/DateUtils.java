package com.li.util;

import cn.hutool.core.date.DateException;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;

import java.lang.management.ManagementFactory;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-25 21:26
 **/
public class DateUtils extends cn.hutool.core.util.StrUtil{
    public static String YYYY = "yyyy";

    public static String YYYY_MM = "yyyy-MM";

    public static String YYYY_MM_DD = "yyyy-MM-dd";

    public static String YYYYMMDDHHMMSS = "yyyyMMddHHmmss";

    public static String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";

    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM",
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

    /**
     * 获取当前时间
     * @return
     */
    public static Date getNowDate()
    {
        Date date = DateUtil.date();
        return date;
    }

    /*时间转换字符串*/
    public static final String dateTimeNow(final String format)
    {
        return parseDateToStr(format, new Date());
    }

    /*时间转换字符串*/
    public static final String parseDateToStr(final String format, final Date date)
    {
        return new SimpleDateFormat(format).format(date);
    }

    /*默认格式*/
    public static final String dateTime(){
        Date date=new Date();
        return DateUtil.format(date,"yyyyMMdd");
    }

    /*字符串日期转换*/
    public static final Date parseDate(Object o){
        if(o == null){
            return null;
        }
        try{
            return DateUtil.parse(o.toString());
        }catch (DateException e){
            return null;
        }
    }

    public static Date getServerStartDate(){
        long time = ManagementFactory.getRuntimeMXBean().getStartTime();
        return new Date(time);
    }

    /*计算天数差*/
    public static String getDatePoor(String date1 , String date2){
        DateTime parse1 = DateUtil.parse(date1);
        DateTime parse2 = DateUtil.parse(date2);

        long betweenDay = DateUtil.between(parse1,parse2, DateUnit.DAY);
        String time = String.valueOf(betweenDay);
        return time;
    }

    /**
     * 计算两个时间差
     */
    public static String getDatePoor(Date endDate, Date nowDate)
    {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;

        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }
}
