package com.li.common.exception;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-12 18:12
 **/
public class UtilException extends RuntimeException{
    public UtilException(Throwable e){
        super(e.getMessage(),e);
    }

    public UtilException(String message){
        super(message);
    }

    public UtilException(String message,Throwable e){
        super(message,e);
    }
}
