package com.li.common.exception;


/**
 * <h3>community</h3>
 * <p>自定义异常</p>
 *
 * @author : 李星鹏
 * @date : 2021-04-16 16:48
 **/
public class CustomException extends RuntimeException{
    private Integer code;
    private String message;

    public CustomException(String message, Integer code)
    {
        this.message = message;
        this.code = code;
    }

    public CustomException(String message, Throwable e)
    {
        super(message, e);
        this.message = message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }

    public Integer getCode()
    {
        return code;
    }
}
