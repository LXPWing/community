package com.li.common.exception;

/**
 * <h3>community</h3>
 * <p>基础异常</p>
 *
 * @author : 李星鹏
 * @date : 2021-04-12 16:05
 **/
public class BaseException extends RuntimeException {
    /**
     * 所属模块
     */
    private String module;

    /**
     * 错误码
     */
    private String code;

    /**
     * 报错参数
     */
    private Object[] args;

    /**
     * 报错信息
     */
    private String defaultMessage;

    public BaseException(String module, String code, Object[] args, String defaultMessage) {
        this.module = module;
        this.code = code;
        this.args = args;
        this.defaultMessage = defaultMessage;
    }

    public BaseException(String module,String code,Object[] objects){
        this(module,code,objects,null);
    }

    public BaseException(String module,String defaultMessage){
        this(module,null,null,defaultMessage);
    }

    public BaseException(String code,Object[] args){
        this(null,code,args,null);
    }

    public BaseException(String module,String code ,String defaultMessage){
        this(module,code,null,defaultMessage);
    }

    public BaseException(String defaultMessage){
        this(null,null,null,defaultMessage);
    }

    @Override
    public String getMessage() {
        return defaultMessage;
    }

    public String getModule() {
        return module;
    }

    public String getCode() {
        return code;
    }

    public Object[] getArgs() {
        return args;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }
}
