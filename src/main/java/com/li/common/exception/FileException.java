package com.li.common.exception;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-16 16:46
 **/
public class FileException extends BaseException {

    public FileException(String code,Object[] args){
        super("file",code,args,null);
    }

}
