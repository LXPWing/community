package com.li.common.domain;

import cn.hutool.http.HttpStatus;
import org.aspectj.weaver.loadtime.Aj;

import java.util.HashMap;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-12 18:30
 **/
public class AjaxResult extends HashMap<String,Object> {
    /** 状态码 */
    private static final String CODE_TAG="code";

    /** 返回内容 */
    private static final String MSG_TAG="msg";

    /** 数据对象 */
    private static final String DATA_TAG="data";

    public AjaxResult(){

    }

    public AjaxResult(int code,String msg){
        super.put(CODE_TAG,code);
        super.put(MSG_TAG,msg);
    }

    public AjaxResult(int code,String msg,Object data){
        super.put(CODE_TAG,code);
        super.put(MSG_TAG,msg);
        if(data!=null){
            super.put(DATA_TAG,data);
        }
    }

    /**
     * 返回正确信息
     * @param msg 返回信息
     * @param data 返回对象
     * @return
     */
    public static AjaxResult success(String msg,Object data){
        return new AjaxResult(HttpStatus.HTTP_OK,msg,data);
    }

    public static AjaxResult success(){
        return AjaxResult.success("操作成功");
    }

    public static AjaxResult success(String msg){
        return AjaxResult.success(msg,null);
    }

    public static AjaxResult success(Object data){
        return AjaxResult.success("操作成功",data);
    }

    /**
     * 返回错误信息
     * @param msg 返回信息
     * @param data 返回对象
     * @return
     */
    public static AjaxResult error(String msg,Object data){
        return new AjaxResult(HttpStatus.HTTP_INTERNAL_ERROR,msg,data);
    }

    public static AjaxResult error(){
        return AjaxResult.error("操作错误");
    }

    public static AjaxResult error(String msg){
        return AjaxResult.error(msg,null);
    }

    public static AjaxResult error(int code,String msg){
        return new AjaxResult(code,msg,null);
    }

}
