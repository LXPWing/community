package com.li.config;

import com.li.util.Threads;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.*;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-04-27 20:22
 **/
@Configuration
public class ThreadPoolConfig {
    private int corePoolSize = 50;

    private int maxPoolSize = 200;

    private int queueCapacity = 1000;

    private int keepAliveSeconds = 300;

    @Bean(name = "threadPoolTaskExecutor")
    public ThreadPoolExecutor threadPoolTaskExecutor(){
        ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
                corePoolSize,
                maxPoolSize,
                keepAliveSeconds,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(queueCapacity),
                new ThreadPoolExecutor.AbortPolicy()
        );
        return threadPoolExecutor;
    }


//    @Bean(name = "scheduledExecutorService")
//    protected ScheduledExecutorService scheduledExecutorService(){
//
//    }
}
