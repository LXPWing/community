package com.li.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import java.util.Properties;

/**
 * <h3>community</h3>
 * <p></p>
 *
 * @author : 李星鹏
 * @date : 2021-05-10 19:36
 **/
@Configuration
public class ScheduleConfig {

    //DataSource dataSource参数待研究
    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(){
        SchedulerFactoryBean factoryBean=new SchedulerFactoryBean();
        //factoryBean.setDataSource(dataSource);
        Properties properties = new Properties();
        properties.put("org.quartz.scheduler.instanceName", "CommunityScheduler");
        properties.put("org.quartz.scheduler.instanceId", "AUTO");
        // 线程池配置
        properties.put("org.quartz.threadPool.class", "org.quartz.simpl.SimpleThreadPool");
        properties.put("org.quartz.threadPool.threadCount", "20");
        properties.put("org.quartz.threadPool.threadPriority", "5");
        // JobStore配置
//        properties.put("org.quartz.jobStore.class", "org.quartz.impl.jdbcjobstore.JobStoreTX");
//        properties.put("org.quartz.jobStore.misfireThreshold", "12000");
//        properties.put("org.quartz.jobStore.tablePrefix", "QRTZ_");

        factoryBean.setQuartzProperties(properties);
        factoryBean.setSchedulerName("Community");
        // 延时启动
        factoryBean.setStartupDelay(1);
        factoryBean.setApplicationContextSchedulerContextKey("applicationContextKey");
        // 可选，QuartzScheduler
        // 启动时更新己存在的Job，这样就不用每次修改targetObject后删除qrtz_job_details表对应记录了
       // factoryBean.setOverwriteExistingJobs(true);
        // 设置自动启动，默认为true
        factoryBean.setAutoStartup(true);

        return factoryBean;
    }
}
